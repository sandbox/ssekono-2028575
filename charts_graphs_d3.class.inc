<?php
/**
 * @file
 * Implementation of abstract class ChartsGraphsCanvas for D3 library.
 */

/**
 * Implementation of abstract class ChartsGraphsCanvas for D3 library.
 */
class ChartsGraphsD3 extends ChartsGraphsCanvas {

  /*
   * Set some defaults
   */
  public $width = 700;
  public $height = 400;
  public $title = '';
  public $series_colours = array('blue', 'red', 'orange', 'green', 'purple');
  public $showlegend = 1;
  public $colour = 'white';

  /**
   * Function that renders data.
   */
  public function get_chart() {

    /*
    * Based on the type of graph, get a default style
    * and set some additional options if we need them
    */
    $legend = array();
    switch ($this->type) {

      case 'LineGraph':
      case 'ColumnChart':
      case 'BarChart':
        $legend = array_keys($this->series);
        $series = $this->series;
        $data = Array();
        $rows = Array();
        foreach ($this->series as $serie) {
          foreach ($serie as $key => $element) {
            $data[$key][] = $element;
          }
        }

        foreach ($this->x_labels as $key => $label) {
          $rows[] = array_merge(array($label), array_values($data[$key]));
        }

      break;

      case 'PieChart':
      case 'DonutChart':

        $pieserie = reset($this->series);
        $i = 0;
        foreach ($pieserie as $serie) {
          $rows[] = array_merge(array($this->x_labels[$i], $serie));
          $i++;
        }

        if (isset($legend)) {
          $chart['legend'] = $legend;
        }

        break;

      case 'BubbleChart':

        $bubbleserie = reset($this->series);
        $i = 0;
        foreach ($bubbleserie as $serie) {
          $child[] = array_merge(array('name' => $this->x_labels[$i],'size' => $serie));
          $i++;
        }

        $jsonArray = array('name' => 'Bubbles', 'children' => $child);

        $rows = drupal_json_encode($jsonArray);

        if (isset($legend)) {
          $chart['legend'] = $legend;
        }

      break;

      default:
        $element = array(
          "#markup" => t("Chart type @name is not supported.", array('@name' => $this->type)),
        );
        return $element;
    }

    $chart = array(
      'id' => 'chart-'.$this->unique_id,
      'type' => $this->type,
      'legend' => $legend,
      'rows' => $rows,
      'width' => $this->width,
      'height' => $this->height,
      'title' => $this->title,
      'series_colours' => $this->series_colours,
      'showlegend' => $this->showlegend,
      'colour' => $this->colour,
    );

    $element = array(
      "#markup" => d3_draw($chart),
    );
    return $element;
  }
}
